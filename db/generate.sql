create or replace schema covidapi;
create or replace table covidapi.countries
(
    country_name    varchar(150)                      not null,
    iso2            char(2) collate latin1_general_cs not null
        primary key,
    new_confirmed   int                               not null,
    total_confirmed int                               not null,
    new_deaths      int                               not null,
    total_deaths    int                               not null,
    new_recovered   int                               not null,
    total_recovered int                               not null
)
    charset = latin1;

