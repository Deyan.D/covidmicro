package com.example.covidmicro.util;

import com.example.covidmicro.models.Country;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class CustomCountryDeserializer extends StdDeserializer<Country> {
    public CustomCountryDeserializer() {
        this(null);
    }

    protected CustomCountryDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Country deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        Country country = new Country();
        ObjectCodec codec = p.getCodec();
        JsonNode node = codec.readTree(p);

        JsonNode countryNode = node.get("Country");
        String countryName = countryNode.asText();
        country.setCountryName(countryName);

        JsonNode countryCodeNode = node.get("CountryCode");
        String countryCode = countryCodeNode.asText();
        country.setIso2(countryCode);

        JsonNode confirmedNewNode = node.get("NewConfirmed");
        int newConfirmed = confirmedNewNode.asInt();
        country.setNewConfirmed(newConfirmed);

        JsonNode confirmedTotalNode = node.get("TotalConfirmed");
        int totalConfirmed = confirmedTotalNode.asInt();
        country.setTotalConfirmed(totalConfirmed);

        JsonNode deathsNewNode = node.get("NewDeaths");
        int newDeaths = deathsNewNode.asInt();
        country.setNewDeaths(newDeaths);

        JsonNode deathsTotalNode = node.get("TotalDeaths");
        int totalDeaths = deathsTotalNode.asInt();
        country.setTotalDeaths(totalDeaths);

        JsonNode recoveredNewNode = node.get("NewRecovered");
        int newRecovered = recoveredNewNode.asInt();
        country.setNewRecovered(newRecovered);

        JsonNode recoveredTotalNode = node.get("TotalRecovered");
        int totalRecovered = recoveredTotalNode.asInt();
        country.setTotalRecovered(totalRecovered);

        return country;
    }
}
