package com.example.covidmicro.util;

import com.example.covidmicro.exceptions.ApiErrorException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class RestServiceCaller {
    private final RestTemplate restTemplate = new RestTemplate();
    private final HttpHeaders httpHeaders = new HttpHeaders();

    public ResponseEntity<String> callRestService(String url, HttpMethod method) {
        try {
            HttpEntity<String> entity = new HttpEntity<>("body", httpHeaders);
            return restTemplate.exchange(url, method, entity, String.class);
        } catch (RestClientException e) {
            throw new ApiErrorException(e.getMessage());
        }
    }
}
