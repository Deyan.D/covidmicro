package com.example.covidmicro.services.mappers;

import com.example.covidmicro.models.Country;
import com.example.covidmicro.models.CountryDtoOut;
import org.springframework.stereotype.Component;

@Component
public class CountryMapper {

    public CountryDtoOut countryToDto(Country country) {
        CountryDtoOut dtoOut = new CountryDtoOut();
        dtoOut.setCountryCode(country.getIso2());
        dtoOut.setName(country.getCountryName());
        dtoOut.setNewConfirmed(country.getNewConfirmed());
        dtoOut.setTotalConfirmed(country.getTotalConfirmed());
        dtoOut.setNewDeaths(country.getNewDeaths());
        dtoOut.setTotalDeaths(country.getTotalDeaths());
        dtoOut.setNewRecovered(country.getNewRecovered());
        dtoOut.setTotalRecovered(country.getTotalRecovered());
        return dtoOut;
    }
}
