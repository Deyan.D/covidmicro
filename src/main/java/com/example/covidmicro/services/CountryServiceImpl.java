package com.example.covidmicro.services;

import com.example.covidmicro.exceptions.EntityNotFoundException;
import com.example.covidmicro.models.Country;
import com.example.covidmicro.repositories.CountryRepository;
import com.example.covidmicro.services.interfaces.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Set<Country> getAll() {
        return new HashSet<>(countryRepository.findAll());
    }

    @Override
    public Country getByIso2(String iso2) {
        Optional<Country> result = countryRepository.findCountryByIso2(iso2);
        return result.orElseThrow(() -> new EntityNotFoundException("Country", iso2));
    }

    @Override
    public int getTotalDeaths() {
        return countryRepository.getTotalDeaths();
    }
}
