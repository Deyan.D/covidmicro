package com.example.covidmicro.services;

import com.example.covidmicro.exceptions.ApiErrorException;
import com.example.covidmicro.models.Country;
import com.example.covidmicro.repositories.CountryRepository;
import com.example.covidmicro.services.interfaces.CovidAPIService;
import com.example.covidmicro.util.CustomCountryDeserializer;
import com.example.covidmicro.util.RestServiceCaller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Objects;


@Service
public class CovidAPIServiceImpl implements CovidAPIService {
    private static final String masterUrl = "https://api.covid19api.com/summary";
    private final CountryRepository countryRepository;
    private final RestServiceCaller apiCaller;
    private final ObjectMapper mapper;

    @Autowired
    public CovidAPIServiceImpl(CountryRepository countryRepository,
                               RestServiceCaller apiCaller, ObjectMapper mapper) {

        this.countryRepository = countryRepository;
        this.apiCaller = apiCaller;
        this.mapper = mapper;
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Country.class, new CustomCountryDeserializer());
        this.mapper.registerModule(module);

    }

    @Override
    public void getCountrySummaries() {
        try {
            ResponseEntity<String> rawEntity = apiCaller.callRestService(masterUrl, HttpMethod.GET);
            if (Objects.requireNonNull(rawEntity.getBody()).contains("Caching in progress")) return;

            String countriesRaw = Objects.requireNonNull(rawEntity.getBody())
                    .substring(rawEntity.getBody().indexOf("["), rawEntity.getBody().indexOf("]") + 1);
            List<Country> countryList = mapper.readValue(countriesRaw, new TypeReference<>() {
            });
            countryRepository.saveAll(countryList);
        } catch (JsonProcessingException | ApiErrorException | IndexOutOfBoundsException e) {
            throw new RuntimeException(e.getMessage());
        }
    }


    @PostConstruct
    public void updateCountriesOnStartup() {
        getCountrySummaries();
    }


    @Scheduled(cron = "*/10 * * * * *", zone = "Europe/Berlin")
    public void updateCountriesOnSchedule() {
        getCountrySummaries();
    }


}
