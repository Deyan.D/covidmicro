package com.example.covidmicro.services.interfaces;

import com.example.covidmicro.models.Country;

import java.util.Set;

public interface CountryService {
    Set<Country> getAll();

    Country getByIso2(String iso2);

    int getTotalDeaths();

}
