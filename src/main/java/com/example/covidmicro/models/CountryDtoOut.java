package com.example.covidmicro.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CountryDtoOut {

    private String countryCode;
    private String name;

    private int newConfirmed;
    private int totalConfirmed;

    private int newDeaths;
    private int totalDeaths;

    private int newRecovered;
    private int totalRecovered;
}
