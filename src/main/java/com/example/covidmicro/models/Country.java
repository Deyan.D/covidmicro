package com.example.covidmicro.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "countries")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Country {

    public Country(String iso2, String countryName, int newConfirmed, int totalConfirmed, int newDeaths, int totalDeaths, int newRecovered, int totalRecovered) {
        this.iso2 = iso2;
        this.countryName = countryName;
        this.newConfirmed = newConfirmed;
        this.totalConfirmed = totalConfirmed;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
    }

    @Id
    @Column(name = "iso2")
    private String iso2;
    @Column(name = "country_name")
    private String countryName;


    @Column(name = "new_confirmed")
    private int newConfirmed;

    @Column(name = "total_confirmed")
    private int totalConfirmed;

    @Column(name = "new_deaths")
    private int newDeaths;

    @Column(name = "total_deaths")
    private int totalDeaths;

    @Column(name = "new_recovered")
    private int newRecovered;

    @Column(name = "total_recovered")
    private int totalRecovered;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return countryName.equals(country.countryName) && iso2.equals(country.iso2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryName, iso2);
    }
}
