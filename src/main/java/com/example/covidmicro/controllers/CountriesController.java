package com.example.covidmicro.controllers;

import com.example.covidmicro.exceptions.EntityNotFoundException;
import com.example.covidmicro.models.CountryDtoOut;
import com.example.covidmicro.services.interfaces.CountryService;
import com.example.covidmicro.services.mappers.CountryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/countries")
public class CountriesController {
    private final CountryService countryService;
    private final CountryMapper mapper;

    @Autowired
    public CountriesController(CountryService countryService, CountryMapper mapper) {
        this.countryService = countryService;
        this.mapper = mapper;
    }

    @GetMapping
    public Set<CountryDtoOut> getAll() {
        return countryService.getAll().stream().map(mapper::countryToDto).collect(Collectors.toSet());
    }

    @GetMapping("/{iso2}")
    public CountryDtoOut getByCountryCode(@PathVariable String iso2) {
        try {
            return mapper.countryToDto(countryService.getByIso2(iso2));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/totaldeaths")
    public String getTotalDeaths() {
        return String.format("\"totalDeaths\": \"%d\"", countryService.getTotalDeaths());
    }
}
