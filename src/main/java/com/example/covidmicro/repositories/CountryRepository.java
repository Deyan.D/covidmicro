package com.example.covidmicro.repositories;

import com.example.covidmicro.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface CountryRepository extends JpaRepository<Country, String> {
    Optional<Country> findCountryByIso2(String iso2);

    @Query("select sum(totalDeaths) from Country")
    int getTotalDeaths();
}
