package com.example.covidmicro.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String type, String iso2) {
        this(type, "country code", iso2);
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found", type, attribute, value));
    }
}
