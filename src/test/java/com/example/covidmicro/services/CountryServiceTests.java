package com.example.covidmicro.services;

import com.example.covidmicro.exceptions.EntityNotFoundException;
import com.example.covidmicro.models.Country;
import com.example.covidmicro.repositories.CountryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class CountryServiceTests {
    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    @Test
    public void getAll_Should_CallRepository() {
        countryService.getAll();
        Mockito.verify(countryRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAll_ShouldNot_Change_CollectionMembers() {
        Country mockCountry1 = new Country("BG", "Bulgaria",
                10, 15,
                20, 25,
                30, 35);

        Country mockCountry2 = new Country("DE", "Germany",
                10, 15,
                20, 25,
                30, 35);

        List<Country> mockList = List.of(mockCountry1, mockCountry2);
        Mockito.when(countryRepository.findAll()).thenReturn(mockList);

        Set<Country> result = countryService.getAll();
        Assertions.assertAll(
                () -> assertEquals(mockList.size(), result.size()),
                () -> assertTrue(result.contains(mockCountry1)),
                () -> assertTrue(result.contains(mockCountry2))
        );
    }

    @Test
    public void getByIso2_Should_CallRepository() {
        Country mockCountry1 = new Country("BG", "Bulgaria",
                10, 15,
                20, 25,
                30, 35);
        Mockito.when(countryRepository.findCountryByIso2(anyString())).thenReturn(Optional.of(mockCountry1));
        countryService.getByIso2("DE");
        Mockito.verify(countryRepository, Mockito.times(1)).findCountryByIso2("DE");
    }

    @Test
    public void getByIso2_Should_Throw_WhenNoCountryIsFound() {
        Mockito.when(countryRepository.findCountryByIso2(anyString())).thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class, () -> countryService.getByIso2(anyString()));
    }
}
