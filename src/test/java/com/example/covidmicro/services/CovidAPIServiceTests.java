package com.example.covidmicro.services;

import com.example.covidmicro.models.Country;
import com.example.covidmicro.repositories.CountryRepository;
import com.example.covidmicro.util.RestServiceCaller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class CovidAPIServiceTests {

    @Mock
    CountryRepository countryRepository;

    @Mock
    RestServiceCaller apiCaller;

    @Mock
    ObjectMapper mapper;

    @InjectMocks
    CovidAPIServiceImpl serviceImpl;


    @Test
    public void getCountrySummaries_Should_NotMapFromJson_If_ExternalApiIsCaching() throws JsonProcessingException {
        String cachingMessage =
                "\"Message\":\"Caching in progress\",\"Global\":[{\"NewConfirmed\":0,\"TotalConfirmed\":0,\"NewDeaths\":0}...";

        ResponseEntity<String> mockEntity = new ResponseEntity<>(cachingMessage, HttpStatus.ACCEPTED);
        Mockito.when(apiCaller.callRestService(anyString(), any(HttpMethod.class))).thenReturn(mockEntity);

        serviceImpl.getCountrySummaries();

        Mockito.verify(mapper, Mockito.times(0)).readValue(anyString(), any(TypeReference.class));
    }

    @Test
    public void getCountrySummaries_Should_CallRepository_WhenSuccessful() throws JsonProcessingException {
        String mockMessage = "[{\"NewConfirmed\":0,\"TotalConfirmed\":0,\"NewDeaths\":0}...]";
        ResponseEntity<String> mockEntity = new ResponseEntity<>(mockMessage, HttpStatus.ACCEPTED);
        List<Country> mockList = List.of(new Country("BG", "Bulgaria",
                        5, 10,
                        15, 20,
                        35, 40),
                new Country("DE", "Germany",
                        5, 10,
                        15, 20,
                        25, 30));
        Mockito.when(apiCaller.callRestService(anyString(), any(HttpMethod.class))).thenReturn(mockEntity);
        Mockito.when(mapper.readValue(anyString(), any(TypeReference.class))).thenReturn(mockList);

        serviceImpl.getCountrySummaries();
        Mockito.verify(countryRepository, Mockito.times(1)).saveAll(mockList);
    }

    @Test
    public void getCountrySummaries_Should_ThrowException_IfInputDoesNotContainJsonArray() {
        String mockMessage = "this is a pointless message";
        ResponseEntity<String> mockEntity = new ResponseEntity<>(mockMessage, HttpStatus.ACCEPTED);
        Mockito.when(apiCaller.callRestService(anyString(), any(HttpMethod.class))).thenReturn(mockEntity);

        Assertions.assertThrows(RuntimeException.class, () -> serviceImpl.getCountrySummaries());
    }


}
