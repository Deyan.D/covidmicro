# Covid data microservice

## Description
Small spring boot 2.7.5 test project, built on Java 11, that calls an external API on startup and every 15 minutes to download the latest global COVID data into a local repository.

If the get method is successful, the microservice's business layer will receive a large JSON array from the API. 

It will then deserialize it into a collection of POJO Country models using a Jackson Databind custom deserializer and if successful saves the countries to the local database.

From then on read operations (either for all countries or by Country Code) are available from a RestController.

The project includes service layer unit tests. 

## Installation
To build the project please first run the SQL generation script that can be found in the DB directory. It will create the schema covidapi with only one table - countries; 

Make sure to set the application.properties file in resources to match your local server's username, password and port you wish to use. Currently it's set to root/root and port 8080;

After that you may run the program. It will automatically call the api "https://api.covid19api.com/summary" on startup and attempt to receive covid data for 197 countries.
The call might not be immediately successful - the api will often be caching new data and the contents will be unavailable.
The microservice will continue to call the API every 10 seconds - in a real work environment this can be changed to every 5-15-30 minutes or even every hour, so as not to carry out expensive I/O operations often.

To change the scheduling setting please go to /services/CovidAPIServiceImpl/updateCountriesOnSchedule() and change its Scheduling cron annotation as is useful.

## Endpoints
This project is locally hosted.

localhost:8080/api/countries - get all country data

localhost:8080/api/countries/{CountryCode} - accepts international ISO2 (case-sensitive all caps) country codes to show individual countries data

localhost:8080/api/countries/totaldeaths - shows the total deaths from covid - was utilized mainly to add a query to the local database

## Technologies Used
Java 11
Spring Boot 2.7.5
Gradle Build Tool
Hibernate
JPA Repositories
Lombok
Jackson Databind
JUnit 5
Mockito

## Acknowledgements

https://covid19api.com/ 

The external API this baby-steps project calls for its data was built by Kyle Redelinghuys and his data is sourced from Johns Hopkins CSSE.